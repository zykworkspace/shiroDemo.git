# shiroDemo

#### 介绍
主要使用了SSM+shiro（maven），实现登录验证和注解权限控制。相当简单的例子

#### 软件架构
软件架构说明


#### 安装教程

1. 执行sql文件下的mysql数据库文件
2. 修改数据库配置appliaction.xml


#### 描述
使用maven，配置启动之后，首先是登录：[http://localhost:8080/page/login](http://)
通过验证后，通过两个按钮反映了权限控制问题。
menu1:注解了存在的权限，可以正常点击获取信息
menu2:注解了不存在的权限，不能正常获取信息，服务端在springmvc中自定义的异常类。