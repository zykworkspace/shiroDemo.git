<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css" href="/js/easyui/themes/default/easyui.css">   
<link rel="stylesheet" type="text/css" href="/js/easyui/themes/icon.css">   
<script type="text/javascript" src="/js/jquery-1.7.2.js"></script>   
<script type="text/javascript" src="/js/easyui/jquery.easyui.min.js"></script>

</head>
<body>
	<div style="width:500px;height:350px;margin:100px auto;">
		<!-- 可能出现id重复的问题. 命名时:文件名_功能 -->
		<div id="login_login" class="easyui-panel"     
				style="width:500px;height:350px;padding:10px;background:#fafafa;" 
		        data-options="iconCls:'icon-cancel',closable:false,title:'登录'">   
		    <div style="text-align:center; font-size:30px; font-weight:bold;padding:10px;">后台管理中心</div> 
			<form id="login_form" method="post" action="/login" style="width:480px; height:220px;text-align:center ">
				<input style="width:400px;margin:10px;height:40px;font-size:26px;" class="easyui-validatebox" type="text" name="username" data-options="required:true,missingMessage:'用户名不能为空'" />  <br/>
				<input style="width:400px;margin:10px;height:40px;font-size:26px;" class="easyui-validatebox" type="password" name="password" data-options="required:true,missingMessage:'密码不能为空'" />  <br/>
				
				<input type="submit" value="sign">  
			</form>
		</div> 
	</div> 
	
	${msg }
</body>
</html>