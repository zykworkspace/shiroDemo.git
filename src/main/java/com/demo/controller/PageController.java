package com.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
@RequestMapping("page")
@Controller
public class PageController {

	
	
	@RequestMapping(value="{pageName}",method = RequestMethod.GET)
	public String getPage(@PathVariable("pageName") String pageName)
	{
		return pageName;
	}
	
}
