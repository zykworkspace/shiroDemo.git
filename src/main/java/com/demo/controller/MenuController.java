package com.demo.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
@RequestMapping("menu")
@Controller
public class MenuController {
	
	@RequiresPermissions("sys:menu:list")
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public String getMenu()
	{
		return "menu";
	}
	
	
	@RequestMapping(value="no",method = RequestMethod.GET)
	@RequiresPermissions("sys:menu:xxx")
	@ResponseBody
	public String getMenu2()
	{
		return "menu2";
	}
	
	
	
}
