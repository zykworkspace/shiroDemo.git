package com.demo.mapper;

import java.util.List;

import com.demo.pojo.SysUserEntity;
import com.github.abel533.mapper.Mapper;

public interface SysUserMapper extends Mapper<SysUserEntity> {

	
	List<String> queryPermsList(Long userid);
}
