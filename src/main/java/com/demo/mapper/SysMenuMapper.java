package com.demo.mapper;

import com.demo.pojo.SysMenuEntity;
import com.github.abel533.mapper.Mapper;

public interface SysMenuMapper extends Mapper<SysMenuEntity> {

}
