package com.demo.mapper;

import com.demo.pojo.SysConfigEntity;
import com.github.abel533.mapper.Mapper;

public interface SysConfigMapper extends Mapper<SysConfigEntity> {

}
