package com.demo.mapper;

import com.demo.pojo.SysUserRoleEntity;
import com.github.abel533.mapper.Mapper;

public interface SysUserRoleMapper extends Mapper<SysUserRoleEntity> {

}
