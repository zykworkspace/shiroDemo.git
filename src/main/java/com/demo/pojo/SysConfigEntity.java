/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.demo.pojo;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 系统配置信息
 *
 * @author Mark sunlightcs@gmail.com
 */

@Table(name = "sys_config")
public class SysConfigEntity {
	@Id
	private Long id;
	
	private String paramKey;
	
	private String paramValue;
	private String remark;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getParamKey() {
		return paramKey;
	}
	public void setParamKey(String paramKey) {
		this.paramKey = paramKey;
	}
	public String getParamValue() {
		return paramValue;
	}
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

}
