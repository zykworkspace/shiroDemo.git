/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.demo.pojo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 菜单管理
 *
 * @author Mark sunlightcs@gmail.com
 */

@Table(name = "sys_menu")
public class SysMenuEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * 菜单ID
	 */
	@Id
	private Long menuId;


	/**
	 * 授权(多个用逗号分隔，如：user:list,user:create)
	 */
	private String perms;

	public Long getMenuId() {
		return menuId;
	}

	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}

	public String getPerms() {
		return perms;
	}

	public void setPerms(String perms) {
		this.perms = perms;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * 类型     0：目录   1：菜单   2：按钮
	 */
	private Integer type;


	}
