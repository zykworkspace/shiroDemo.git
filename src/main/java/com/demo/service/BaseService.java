package com.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.github.abel533.entity.Example;
import com.github.abel533.mapper.Mapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

public class BaseService<T> {

	/*
	 * spring4支持泛型注入
	 * 
	 * */
	
	@Autowired
	private Mapper<T> mapper;
	
	/*
	 * 1、queryById
2、queryAll
3、queryOne
4、queryListByWhere
5、queryPageListByWhere
6、save
7、update
8、deleteById
9、deleteByIds
10、deleteByWhere
	 * 
	 * 
	 * */
	public Integer deleteByWhere(T record)
	{
		return this.mapper.delete(record);
	}
	
	public Integer deleteByIds(Class<T> clazz,String properties ,List<Object> values)
	{
		Example ex = new Example(clazz);
		ex.createCriteria().andIn(properties, values);
		return this.mapper.deleteByExample(ex);
	}
	
	public int deleteById(Long id)
	{
		return this.mapper.deleteByPrimaryKey(id);
	}
	
	public int update(T record)
	{
		
		return this.mapper.updateByPrimaryKey(record);
	}
	public int updateSelective(T record)
	{
		
		return this.mapper.updateByPrimaryKeySelective(record);
	}
	
	public int saveSelective(T record)
	{
	
		return this.mapper.insertSelective(record);
	}
	
	
	public int save(T record)
	{
		return this.mapper.insert(record);
	}
	
	//load page
	public PageInfo<T> queryPageListByWhere(Integer page,Integer rows,T record)
	{
		PageHelper.startPage(page, rows);
		List<T> list = this.queryListByWhere(record);
		return new PageInfo<T>(list);
	}
	
	public List<T> queryListByWhere(T record)
	{
		return this.mapper.select(record);
	}
	
	
	public T queryById(Long id)
	{
		return this.mapper.selectByPrimaryKey(id);
	}
	public List<T> queryAll()
	{
		return this.mapper.selectByExample(null);
	}
	public T queryOne(T record)
	{
		return this.mapper.selectOne(record);
	}
	
	
}
