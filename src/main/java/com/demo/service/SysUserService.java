package com.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.mapper.SysUserMapper;
import com.demo.pojo.SysUserEntity;
@Service
public class SysUserService extends BaseService<SysUserEntity> {

	
	@Autowired
	private SysUserMapper sysUserMapper;
	/**
	 * 查询用户的所有权限
	 * @param userId  用户ID
	 */
	public List<String> queryAllPerms(Long userId){
			List<String> perms = this.sysUserMapper.queryPermsList(userId);
			if (perms != null) {
				return perms;
			}
		return null;
		
	}
	
	
}
