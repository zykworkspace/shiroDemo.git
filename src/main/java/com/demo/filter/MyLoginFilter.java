package com.demo.filter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;

public class MyLoginFilter extends FormAuthenticationFilter{
	@Override
	protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
		//此处可以加入自己的验证逻辑，比如验证码
		return super.onAccessDenied(request, response);
		
	}
}
